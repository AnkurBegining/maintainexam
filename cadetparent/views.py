from django.shortcuts import render, redirect, get_object_or_404
from employee.models import StudentProfile
from exam.models import Student_Exam_Record
from maintainexam.decorators import user_is_cadet_or_parent


# Create your views here.
@user_is_cadet_or_parent
def cadet_parent_profile(request):
    print("Request user", request.user.username)
    sc_num = request.user.username
    school_num = int(sc_num.split("_")[1])
    print("School_Num", school_num)
    cadet_record = get_object_or_404(StudentProfile, Roll_Num=school_num)
    print("Cadet_record", cadet_record.id)
    exam_record1 = Student_Exam_Record.objects.filter(Student=cadet_record, Exam='Class Test').order_by('Subject')
    exam_record2 = Student_Exam_Record.objects.filter(Student=cadet_record, Exam='Periodic Test 1').order_by(
        'Subject')
    exam_record3 = Student_Exam_Record.objects.filter(Student=cadet_record,
                                                      Exam='Half Yearly  Examination/ Periodic Test 2').order_by(
        'Subject')
    exam_record4 = Student_Exam_Record.objects.filter(Student=cadet_record,
                                                      Exam='Periodic Test 2 / Periodic Test 3').order_by('Subject')
    exam_record5 = Student_Exam_Record.objects.filter(Student=cadet_record,
                                                      Exam='Session Ending Examination').order_by('Subject')
    context = {
        'student_profile': cadet_record,
        'examrecord1': exam_record1,
        'examrecord2': exam_record2,
        'examrecord3': exam_record3,
        'examrecord4': exam_record4,
        'examrecord5': exam_record5,
    }
    return render(request, 'employee/student_detail.html', context)
