from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class CadetParent(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="CadetParent")
    is_cadet_parent = models.BooleanField(default=False)

    def __str__(self):
        return str(self.user)


