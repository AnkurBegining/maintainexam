from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from django.conf import settings
from cadetparent import views


urlpatterns = [
    url(r'^cadet_parent_profile/$', views.cadet_parent_profile, name='cadet_parent_profile'),
]