from django.apps import AppConfig


class CadetparentConfig(AppConfig):
    name = 'cadetparent'
