from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from django.conf import settings
from employee import views

urlpatterns = [
    url(r'^employee_profile/$', views.employee_profile, name='employee_profile'),
    url(r'^create_student_record/$', views.create_student_record, name='create_student_record'),
    url(r'^student_detail/(?P<pk>\d+)$', views.student_detail, name='student_detail'),
    url(r'^edit_student_record/(?P<pk>\d+)$', views.edit_student_record, name='edit_student_record')

]
