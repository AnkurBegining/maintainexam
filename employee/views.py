from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from employee.models import StudentProfile, StudentEditLog, Employee
from employee.forms import StudentProfileAddForm
from maintainexam.decorators import user_is_employee
from exam.models import Student_Exam_Record, Mid_Term_Exam_Record_Senior, Mid_Term_Exam_Record_Junior
from django.db.models import Q
from django.contrib.auth.models import User



# Create your views here.
@user_is_employee
def employee_profile(request):
    students = StudentProfile.objects.all()
    query = request.GET.get("q")

    if query:
        students = students.filter(Q(Roll_Num__icontains=query) |
                                             Q(Cadet_Name__icontains=query) |
                                             Q(Class__icontains=query) |
                                             Q(House__icontains=query)
                                             ).distinct().order_by('Cadet_Name')
    # else:
    #     students = StudentProfile.objects.none()
    # for larger inputs
    context = {

        'student_profile': students
    }
    # print("Request User", request.user.id)
    # user = Employee.objects.get(user_id=request.user.id)
    # print("User", user)
    return render(request, 'employee/employee_profile.html', context)


@user_is_employee
def create_student_record(request):
    print("Create Student Record")
    if request.method == "POST":
        add_form = StudentProfileAddForm(request.POST or None, request.FILES or None)
        if add_form.is_valid():
            instance = add_form.save(commit=False)
            instance.save()
            # user = Employee.objects.get(user_id=request.user.id)
            # print("instance", instance.pk)
            # student_profile = StudentProfile.objects.get(id=instance.pk)
            # print("User", user)
            # print("Student Profile Id", instance.pk)
            # StudentEditLog.objects.create(employee_id=user, student_profile_id=student_profile)

            print("Successfully Saved")
            return redirect('employee:employee_profile')
        else:
            arr = []
            for field in add_form:
                arr.append(field.errors)
            messages.error(request, add_form.errors)
            add_form = StudentProfileAddForm(request.POST or None, request.FILES or None)
    else:
        add_form = StudentProfileAddForm()

    context = {
        'add_form': add_form
    }
    return render(request, 'employee/create_student_record.html', context)


@user_is_employee
def student_detail(request, pk):
    student_profile = get_object_or_404(StudentProfile, id=pk)
    exam_record1 = Student_Exam_Record.objects.filter(Student=student_profile, Exam='Periodic Test 1').order_by('Subject')
    exam_record3 = Student_Exam_Record.objects.filter(Student=student_profile,
                                                      Exam='Periodic Test 2').order_by('Subject')
    exam_record4 = Student_Exam_Record.objects.filter(Student=student_profile,
                                                      Exam='Pre Mid Term Examination').order_by('Subject')
    print(int(student_profile.Class))
    if 6 <= int(student_profile.Class) <= 10:
        print(int(student_profile.Class))
        exam_record2 = Mid_Term_Exam_Record_Junior.objects.filter(Student=student_profile, Exam='Mid Term Exam').order_by('Subject')
        print(exam_record2,"asd")
        exam_record5 = Mid_Term_Exam_Record_Junior.objects.filter(Student=student_profile, Exam='Post Mid Term Examination').order_by('Subject')
    else:
        exam_record2 = Mid_Term_Exam_Record_Senior.objects.filter(Student=student_profile,
                                                                  Exam='Mid Term Exam').order_by('Subject')
        exam_record5 = Mid_Term_Exam_Record_Senior.objects.filter(Student=student_profile,
                                                                  Exam='Post Mid Term Examination').order_by('Subject')

    print(exam_record2)
    context={
        'student_profile': student_profile,
        'examrecord1': exam_record1,
        'examrecord2': exam_record2,
        'examrecord3': exam_record3,
        'examrecord4': exam_record4,
        'examrecord5': exam_record5,
    }

    return render(request, 'employee/student_detail.html', context)


@user_is_employee
def edit_student_record(request, pk):
    student_profile = get_object_or_404(StudentProfile, id=pk)

    if request.method == "POST":
        add_form = StudentProfileAddForm(request.POST or None, request.FILES or None, instance=student_profile)
        if add_form.is_valid() == True:
            instance = add_form.save(commit=False)
            instance.save()
            print(instance)
            # user = Employee.objects.get(user_id=request.user.id)
            # print("instance", instance.pk)
            # student_profile = StudentProfile.objects.get(id=instance.pk)
            # print("User", user)
            # print("Student Profile Id", instance.pk)
            # StudentEditLog.objects.create(employee_id=user, student_profile_id=student_profile)
            print("Successfully Saved")
            return redirect('employee:employee_profile')
        else:
            arr = []
            for field in add_form:
                arr.append(field.errors)
            messages.error(request, add_form.errors)

    else:
        add_form = StudentProfileAddForm(request.POST or None, request.FILES or None, instance=student_profile)

    context = {
        'add_form': add_form
    }
    return render(request, 'employee/create_student_record.html', context)
