from django.contrib import admin
from employee.models import Employee, StudentProfile, StudentEditLog
# Register your models here.

admin.site.register(Employee)
admin.site.register(StudentProfile)
admin.site.register(StudentEditLog)