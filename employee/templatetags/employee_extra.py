from django import template

register = template.Library()


@register.filter("replace")
def replace(string):
    return string.replace('_', ' ')
