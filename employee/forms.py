from django import forms
from employee.models import StudentProfile

class DateInput(forms.DateInput):
    input_type = 'date'


class StudentProfileAddForm(forms.ModelForm):
    class Meta:
        model = StudentProfile
        fields = '__all__'
        widgets = {
            'Date_Of_Birth': DateInput(),
            'Date_Of_Arrival': DateInput(),
            'Date_Of_Withdrawal' : DateInput(),
            'Date': DateInput(),
            'TC_No_Issued_Date': DateInput()
        }

