from django.contrib.auth.models import User
from django.db import models
from datetime import datetime
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import MinLengthValidator, MaxLengthValidator
from django_currentuser.middleware import (
    get_current_user, get_current_authenticated_user)

# Create your models here.
from maintainexam.settings import TIME_ZONE


class Employee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="Employee")
    is_employee = models.BooleanField(default=False)

    def __str__(self):
        return str(self.user)


class StudentProfile(models.Model):
    CLASS_CHOICE = (
        [(str(i), str(i)) for i in range(6, 13)]
    )
    SECTION_CHOICE = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
        ('E', 'E'),
        ('F', 'F'),
    )
    HOUSE_CHOICE = (
        ('Chera Junior', 'Chera Junior'), ('Chola Junior', 'Chola Junior'),
        ('Pandya Junior', 'Pandya Junior'), ('Pallava Junior', 'Pallava Junior'),
        ('Pandya Senior', 'Pandya Senior'),('Chera Senior', 'Chera Senior'),
        ('Chola Senior', 'Chola Senior'), ('Pallava Senior', 'Pallava Senior'),
        ('Bharati', 'Bharati'), ('Velluvar', 'Velluvar'),

    )
    lIISubject = (
        ('Tamil', 'Tamil'), ('Hindi', 'Hindi'), ('Biology', 'Biology'), ('CS', 'CS'),
    )
    Roll_Num = models.IntegerField(unique=True)
    Cadet_Name = models.CharField(max_length=250)
    Class = models.CharField(max_length=2, choices=CLASS_CHOICE, default="--select--")
    Section = models.CharField(max_length=1, choices=SECTION_CHOICE, default="--select--")
    House = models.CharField(max_length=12, choices=HOUSE_CHOICE, default="--select--")
    Aadhar_Num = models.IntegerField()
    EMIS_Num = models.IntegerField()
    Date_Of_Birth = models.DateField()
    Domicile = models.CharField(max_length=250)
    Father_Name = models.CharField(max_length=250)
    Mobile_Num = models.IntegerField()
    Additional_Contact_Num_1 = models.IntegerField(blank=True, null=True)
    Additional_Contact_Num_2 = models.IntegerField(blank=True, null=True)
    Mother_Name = models.CharField(max_length=250)
    Class_Of_Admission = models.CharField(max_length=250)
    Address = models.TextField()
    Father_Occupation = models.CharField(max_length=250)
    Monthly_Income = models.IntegerField()
    Category = models.CharField(max_length=250)
    Background = models.CharField(max_length=250, blank=True, null=True)
    Community = models.CharField(max_length=250, blank=True, null=True)
    Caste = models.TextField()
    Identification_Mark = models.TextField(blank=True, null=True)
    School_Last_Studied = models.CharField(max_length=350)
    Date_Of_Arrival = models.DateField()
    TC_No_Last_Studied = models.CharField(max_length=550, blank=True, null=True)
    TC_No_Issued_Date = models.DateField(blank=True, null=True)
    Date_Of_Withdrawal = models.DateField(blank=True, null=True)
    TC_No_Sainik_School = models.CharField(max_length=550, blank=True, null=True)
    # To-do
    Date = models.DateField(blank=True, null=True)
    Remark = models.TextField(blank=True, null=True)
    Total_Num_of_sister = models.IntegerField(blank=True, null=True)
    Total_Num_of_Brother = models.IntegerField(blank=True, null=True)
    # To-do
    Photograph_Cadet = models.ImageField(
        null=True, blank=True,
        height_field="height_field",
        width_field="width_field")
    height_field = models.IntegerField(default=None, blank=True, null=True)
    width_field = models.IntegerField(default=None, blank=True, null=True)
    NDA_Qaulified = models.CharField(max_length=250, default=None, blank=True, null=True)
    NDA_All_India_Rank = models.IntegerField(default=None, blank=True, null=True)
    Elective_Subject = models.CharField(max_length=12, choices=lIISubject, null=True, blank=True)


    def __str__(self):
        return str(self.Roll_Num)

    def get_fields(self):
        return [(field.name, field.value_to_string(self)) for field in StudentProfile._meta.fields]


class StudentEditLog(models.Model):
    employee_id = models.ForeignKey(Employee, on_delete=models.DO_NOTHING)
    student_profile_id = models.ForeignKey(StudentProfile, on_delete=models.DO_NOTHING)
    timestamp = models.DateTimeField(default=datetime.now())

    def __str__(self):
        return str(self.employee_id.user)
        # + ', ' + str(self.student_profile_id) + ', ' + str(self.timestamp.date()) + ', ' + str(self.timestamp.time())

    @receiver(post_save, sender=StudentProfile)
    def create_EditLog(sender, instance, created, **kwargs):
        user = Employee.objects.get(user_id=get_current_authenticated_user().id)
        StudentEditLog.objects.create(employee_id=user, student_profile_id=instance)

    post_save.connect(create_EditLog, sender=StudentProfile)
