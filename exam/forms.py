from django import forms
from exam.models import Teacher, Student_Exam_Record, Mid_Term_Exam_Record_Junior, Mid_Term_Exam_Record_Senior
from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet


class DateInput(forms.DateInput):
    input_type = 'date'


class ExamRecordForm1(forms.Form):
    CLASS_CHOICES = (
        [(str(i), str(i)) for i in range(6, 13)]
    )
    SECTION_CHOICE = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
        ('E', 'E'),
        ('F', 'F'),
    )
    EXAM_CHOICES = (
        ('Periodic Test 1', 'Periodic Test 1'),
        ('Mid Term Exam', 'Mid Term Exam'),
        ('Periodic Test 2 ', 'Periodic Test 2'),
        ('Pre Mid Term Examination', 'Pre Mid Term Examination'),
        ('Post Mid Term Examination', 'Post Mid Term Examination'),
    )
    SUBJECT_CHOICES = (
        ('English', 'English'),
        ('Hindi', 'Hindi'),
        ('Tamil', 'Tamil'),
        ('Maths', 'Maths'),
        ('General Science', 'General Science'),
        ('Social Science', 'Social Science'),
        ('Computer Application-I', 'Computer Application-I'), ('Computer Application-II', 'Computer Application-II'),
        ('Physics', 'Physics'), ('Chemistry', 'Chemistry'), ('Biology', 'Biology'),
        ('Computer', 'Computer')
    )
    Select_Class = forms.ChoiceField(choices=CLASS_CHOICES, required=True)
    Section = forms.ChoiceField(choices=SECTION_CHOICE, required=True)
    Exam = forms.ChoiceField(choices=EXAM_CHOICES, required=True)
    Subject = forms.ChoiceField(choices=SUBJECT_CHOICES, required=True)
    Max_Marks = forms.IntegerField(required=True)
    Exam_Date = forms.DateField(widget=DateInput(), required=True)
    Class_Teacher = forms.ChoiceField(required=True)

    def __init__(self, *args, **kwargs):
        super(ExamRecordForm1, self).__init__(*args, **kwargs)

        self.fields['Class_Teacher'] = forms.ChoiceField(
            choices=[(teacher.Teacher_Name, teacher.Teacher_Name) for teacher in
                     Teacher.objects.all().order_by('Teacher_Name')]
        )


class ExamRecordForm2(forms.Form):
    Marks = forms.IntegerField(required=True)
    Grade = forms.CharField(max_length=3)
    Percentage = forms.DecimalField(max_digits=5,decimal_places=2,required=True)
    Rank = forms.IntegerField(required=False)
    Max_marks = forms.DecimalField(widget=forms.HiddenInput(),max_digits=5, decimal_places=2)

    def clean(self):
        if self.cleaned_data['Marks'] > self.cleaned_data['Max_marks']:
            raise ValidationError(
                "{} is invalid, must be less than or equal to Max_Marks {}".format(self.cleaned_data['Marks'],
                                                                                   self.cleaned_data['Max_marks']))


class StudentExamEditForm(forms.ModelForm):
    Exam_Date = forms.DateField(widget=DateInput())

    class Meta:
        model = Student_Exam_Record
        fields = '__all__'
        exclude = ['Student']

    def clean(self):
        # print("clean")
        if self.cleaned_data['Marks'] > self.cleaned_data['Max_Marks']:
            raise ValidationError(
                "{} is invalid, must be less than or equal to Max_Marks {}".format(self.cleaned_data['Marks'],
                                                                                   self.cleaned_data['Max_Marks']))


class MidTermExamEditFormJunior(forms.ModelForm):
    Exam_Date = forms.DateField(widget=DateInput())

    class Meta:
        model = Mid_Term_Exam_Record_Junior
        fields = '__all__'
        exclude = ['Student']

    def clean(self):
        # print("clean")
        if self.cleaned_data['Theory_Exam_Marks'] > 80.00 or self.cleaned_data['NBS_Marks'] > 5.00 or self.cleaned_data[
            'PT1_Exam_Marks'] > 5.00 or self.cleaned_data['SEA_Marks'] > 5.00 or self.cleaned_data['Percentage'] > 100.00:
            raise ValidationError(
                "Input Marks is invalid, must be less than or equal to Max_Marks")


class MidTermExamEditFormSenior(forms.ModelForm):
    Exam_Date = forms.DateField(widget=DateInput())

    class Meta:
        model = Mid_Term_Exam_Record_Senior
        fields = '__all__'
        exclude = ['Student']

    def clean(self):
        # print("clean")
        if self.cleaned_data['Theory_Marks'] > 70.00 or self.cleaned_data['Practical_Marks'] > 30.00 or self.cleaned_data['Percentage'] > 100.00:
            raise ValidationError(
                "Input Marks is invalid, must be less than or equal to Max_Marks")

# class StudentExamFormSet(BaseInlineFormSet):
#     def clean(self):
#         super().clean()
#         for form in self.forms:
#             print("clean")
#             if form.cleaned_data['Marks'] > form.cleaned_data['Max_marks']:
#                 raise ValidationError(
#                     "{} is invalid, must be less than or equal Max_Marks {}".format(form.cleaned_data['Marks'],
#                                                                                     form.cleaned_data['Max_marks']))
