from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .forms import ExamRecordForm1, ExamRecordForm2, StudentExamEditForm, DateInput, MidTermExamEditFormJunior, MidTermExamEditFormSenior
from employee.models import StudentProfile
from .models import Student_Exam_Record, Mid_Term_Exam_Record_Senior, Mid_Term_Exam_Record_Junior
from django.forms import formset_factory, inlineformset_factory
from maintainexam.decorators import user_is_employee
from django.contrib import messages
import datetime, urllib.parse


# Create your views here.

def home(request):
    return render(request, 'exam/index.html', {'len': range(4)})


@user_is_employee
def create_exam_record(request):
    print("Create Student Record")

    if request.method == "POST" and 'form1' in request.POST:
        record_form = ExamRecordForm1(request.POST or None, request.FILES or None)
        if record_form.is_valid():
            print("Successfully Saved")
            return HttpResponseRedirect("/save_exam_records/?%s" % urllib.parse.urlencode(record_form.cleaned_data))

    else:
        record_form = ExamRecordForm1()

    context = {'record_form': record_form, }
    return render(request, 'exam/create_exam_record.html', context)


@user_is_employee
def save_exam_records(request):
    print("data")
    data = request.GET
    if data['Subject'] == 'Hindi' or data['Subject'] == 'Tamil' or data['Subject'] == 'CS' or data[
        'Subject'] == 'Biology':
        students = StudentProfile.objects.filter(Class=data['Select_Class'], Section=data['Section'],
                                                 Elective_Subject=data['Subject']).order_by('Roll_Num')
    else:
        students = StudentProfile.objects.filter(Class=data['Select_Class'], Section=data['Section']).order_by(
            'Roll_Num')

    ExamRecordFormSet1 = formset_factory(ExamRecordForm2, extra=students.count())
    ExamRecordFormSet2 = inlineformset_factory(StudentProfile, Mid_Term_Exam_Record_Junior, fields=(
        'Theory_Exam_Marks', 'NBS_Marks', 'SEA_Marks', 'PT1_Exam_Marks', 'Grade', 'Percentage', 'Rank'))
    ExamRecordFormSet3 = inlineformset_factory(StudentProfile, Mid_Term_Exam_Record_Senior,
                                               fields=('Theory_Marks', 'Practical_Marks', 'Percentage', 'Rank'))

    if request.method == "POST" and 'form2' in request.POST:

        if 6 <= int(data['Select_Class']) <= 10 and data['Exam'] == 'Mid Term Exam' or 6 <= int(
                data['Select_Class']) <= 10 and data['Exam'] == 'Post Mid Term Examination':
            formset = ExamRecordFormSet2(request.POST or None, prefix='formset2')
        elif 11 <= int(data['Select_Class']) <= 12 and data['Exam'] == 'Mid Term Exam' or 11 <= int(
                data['Select_Class']) <= 12 and data['Exam'] == 'Post Mid Term Examination':
            formset = ExamRecordFormSet3(request.POST or None, prefix='formset3')
        else:
            formset = ExamRecordFormSet1(request.POST or None, prefix='formset1')
        print('post')
        if formset.is_valid():
            combine = zip(students, formset)
            if formset.prefix == 'formset1':
                for student, form in combine:
                    print('valid')
                    student_exam_record = Student_Exam_Record.objects.create(Student=student,
                                                                             Exam=data['Exam'],
                                                                             Exam_Date=data['Exam_Date'],
                                                                             Subject=data['Subject'],
                                                                             Grade=form.cleaned_data['Grade'],
                                                                             Marks=form.cleaned_data['Marks'],
                                                                             Rank=form.cleaned_data['Rank'],
                                                                             Max_Marks=data['Max_Marks'],
                                                                             Class_Teacher=data['Class_Teacher'])
                    student_exam_record.save()

            if formset.prefix == 'formset2':
                for student, form in combine:
                    student_exam_record = Mid_Term_Exam_Record_Junior.objects.create(Student=student,
                                                                                     Exam=data['Exam'],
                                                                                     Exam_Date=data['Exam_Date'],
                                                                                     Subject=data['Subject'],
                                                                                     Theory_Exam_Marks=
                                                                                     form.cleaned_data[
                                                                                         'Theory_Exam_Marks'],
                                                                                     NBS_Marks=form.cleaned_data[
                                                                                         'NBS_Marks'],
                                                                                     SEA_Marks=form.cleaned_data[
                                                                                         'SEA_Marks'],
                                                                                     PT1_Exam_Marks=form.cleaned_data[
                                                                                         'PT1_Exam_Marks'],
                                                                                     Percentage=form.cleaned_data[
                                                                                         'Percentage'],
                                                                                     Grade=form.cleaned_data['Grade'],
                                                                                     Rank=form.cleaned_data['Rank'],
                                                                                     )

                    student_exam_record.save()

            if formset.prefix == 'formset3':
                for student, form in combine:
                    print('valid3')
                    student_exam_record = Mid_Term_Exam_Record_Senior.objects.create(Student=student,
                                                                                     Exam=data['Exam'],
                                                                                     Exam_Date=data['Exam_Date'],
                                                                                     Subject=data['Subject'],
                                                                                     Percentage=form.cleaned_data[
                                                                                         'Percentage'],
                                                                                     Theory_Marks=form.cleaned_data[
                                                                                         'Theory_Marks'],
                                                                                     Practical_Marks=form.cleaned_data[
                                                                                         'Practical_Marks'],
                                                                                     Rank=form.cleaned_data['Rank'],
                                                                                     )
                    student_exam_record.save()

                    print("Successfully Saved again")
            return redirect('employee:employee_profile')
        else:
            print(formset.errors)
    else:

        if 6 <= int(data['Select_Class']) <= 10 and data['Exam'] == 'Mid Term Exam' or 6 <= int(
                data['Select_Class']) <= 10 and \
                data['Exam'] == 'Post Mid Term Examination':
            formset = ExamRecordFormSet2(prefix='formset2')
        elif 11 <= int(data['Select_Class']) <= 12 and data['Exam'] == 'Mid Term Exam' or 11 <= int(
                data['Select_Class']) <= 12 and data[
            'Exam'] == 'Post Mid Term Examination':
            formset = ExamRecordFormSet3(prefix='formset3')
        else:
            formset = ExamRecordFormSet1(prefix='formset1')

    combine = zip(students, formset)
    context = {

        'Max_marks': data['Max_Marks'],
        'formset': formset,
        'combine': combine,

    }
    return render(request, 'exam/save_exam_record.html', context)

@user_is_employee
def student_exam_edit(request, pk):
    student_profile = StudentProfile.objects.get(pk=pk)

    exam_record1 = Student_Exam_Record.objects.filter(Student=student_profile, Exam='Periodic Test 1').order_by(
        'Subject')
    exam_record3 = Student_Exam_Record.objects.filter(Student=student_profile,
                                                      Exam='Periodic Test 2').order_by('Subject')
    exam_record4 = Student_Exam_Record.objects.filter(Student=student_profile,
                                                      Exam='Pre Mid Term Examination').order_by('Subject')

    if 6 <= int(student_profile.Class) <= 10:

        exam_record2 = Mid_Term_Exam_Record_Junior.objects.filter(Student=student_profile,
                                                                  Exam='Mid Term Exam').order_by('Subject')

        exam_record5 = Mid_Term_Exam_Record_Junior.objects.filter(Student=student_profile,
                                                                  Exam='Post Mid Term Examination').order_by('Subject')
    else:
        exam_record2 = Mid_Term_Exam_Record_Senior.objects.filter(Student=student_profile,
                                                                  Exam='Mid Term Exam').order_by('Subject')
        exam_record5 = Mid_Term_Exam_Record_Senior.objects.filter(Student=student_profile,
                                                                  Exam='Post Mid Term Examination').order_by('Subject')

    StudentExamEditFormSet1 = inlineformset_factory(StudentProfile, Student_Exam_Record, form=StudentExamEditForm,
                                                    fields=('Class_Teacher', 'Exam', 'Max_Marks', 'Subject', 'Exam_Date', 'Marks', 'Percentage', 'Grade', 'Rank'),
                                                    extra=0, widgets={'Exam_Date': DateInput()})

    if 6 <= int(student_profile.Class) <= 10:
        StudentExamEditFormSet2 = inlineformset_factory(StudentProfile, Mid_Term_Exam_Record_Junior, form=MidTermExamEditFormJunior,
                                                        fields=('Subject', 'Exam_Date', 'Theory_Exam_Marks', 'NBS_Marks', 'SEA_Marks','PT1_Exam_Marks', 'Percentage', 'Grade', 'Rank'),
                                                        extra=0, widgets={'Exam_Date': DateInput()})
    else:
        StudentExamEditFormSet2 = inlineformset_factory(StudentProfile, Mid_Term_Exam_Record_Senior, form=MidTermExamEditFormSenior,
                                                    fields=('Subject', 'Exam_Date', 'Theory_Marks', 'Practical_Marks', 'Percentage', 'Rank'), extra=0,
                                                    widgets={'Exam_Date': DateInput()})
    if request.method == "POST":
        formset1 = StudentExamEditFormSet1(request.POST or None, request.FILES or None, instance=student_profile, prefix='student_exam_record1')
        formset2 = StudentExamEditFormSet2(request.POST or None, request.FILES or None, instance=student_profile, prefix='student_exam_record2')

        if formset1.is_valid() and formset2.is_valid():
            formset1.save()
            formset2.save()
            return redirect('employee:employee_profile')
        else:
            print(formset2.errors)


    else:
        formset1 = StudentExamEditFormSet1(instance=student_profile, prefix='student_exam_record1')
        formset2 = StudentExamEditFormSet2(instance=student_profile, prefix='student_exam_record2')

    context = {'formset1':formset1,
               'formset2': formset2,
               'examrecord1': exam_record1,
               'examrecord2': exam_record2,
               'examrecord3': exam_record3,
               'examrecord4': exam_record4,
               'examrecord5': exam_record5,
               'student_profile':student_profile
               }

    return render(request, 'exam/student_exam_edit.html', context)
