from django.db import models
from employee.models import StudentProfile
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator, MaxLengthValidator
import datetime

# Create your models here.

class Teacher(models.Model):# To be set by Admin
    CLASS_CHOICE = (
        [(str(i), str(i)) for i in range(6, 13)]
    )
    SECTION_CHOICE = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
        ('E', 'E'),
        ('F', 'F'),
    )
    Teacher_Name = models.CharField(max_length=15)
    is_class_Teacher = models.BooleanField(default=False)
    Class = models.CharField(max_length=1, choices=CLASS_CHOICE, default="--select--", null=True, blank=True)
    Section = models.CharField(max_length=1, choices=SECTION_CHOICE, default="--select--", null=True, blank=True)


    def __str__(self):
        return str(self.Teacher_Name)
        # + ', ' + str(self.student_profile_id) + ', ' + str(self.timestamp.date()) + ', ' + str(self.timestamp.time())

class Student_Exam_Record(models.Model):
    Student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE)
    Class_Teacher = models.CharField(max_length=20)
    Max_Marks = models.DecimalField(max_digits=5, decimal_places=2)
    Exam = models.CharField(max_length=20)
    Exam_Date = models.DateField(default=datetime.date.today)
    Subject = models.CharField(max_length=20)
    Marks = models.DecimalField(max_digits=5, decimal_places=2)
    Percentage = models.DecimalField(max_digits=5, decimal_places=2, default=0)
    Grade = models.CharField(max_length=3, null=True, blank=True)
    Rank = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return str(self.Student.Roll_Num) + ", " +str(self.Student.Class) + ", " + str(self.Student.Section)

    def clean(self):
        if self.Marks > int(self.Max_Marks):
            raise ValidationError("{} is invalid, must be less than or equal {}".format(self.Marks,self.Max_Marks))

class Mid_Term_Exam_Record_Junior(models.Model):
    Student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE)
    Exam = models.CharField(max_length=20)
    Exam_Date = models.DateField(default=datetime.date.today)
    Subject = models.CharField(max_length=20)
    Theory_Exam_Marks = models.DecimalField(max_digits=5, decimal_places=2)
    NBS_Marks = models.DecimalField(max_digits=5, decimal_places=2)
    SEA_Marks = models.DecimalField(max_digits=5, decimal_places=2)
    PT1_Exam_Marks = models.DecimalField(max_digits=5, decimal_places=2)
    Percentage = models.DecimalField(max_digits= 5, decimal_places=2)
    Grade = models.CharField(max_length=3)
    Rank = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.Student.Roll_Num) + ", " +str(self.Student.Class) + ", " + str(self.Student.Section)

    def clean(self):
        if self.Theory_Exam_Marks > 80.00 or self.NBS_Marks > 5.00 or self.PT1_Exam_Marks > 10.00 or self.SEA_Marks > 5.00 or self.Percentage > 100.00:
            raise ValidationError(
                "Input Marks is invalid, must be less than or equal to Max_Marks")



class Mid_Term_Exam_Record_Senior(models.Model):
    Student = models.ForeignKey(StudentProfile, on_delete=models.CASCADE)
    Exam = models.CharField(max_length=20)
    Exam_Date = models.DateField(default=datetime.date.today)
    Subject = models.CharField(max_length=20)
    Theory_Marks = models.DecimalField(max_digits=5, decimal_places=2)
    Practical_Marks = models.DecimalField(max_digits=5, decimal_places=2)
    Percentage = models.DecimalField(max_digits=5, decimal_places=2)
    Rank = models.IntegerField(null=True, blank=True)


    def __str__(self):
        return str(self.Student.Roll_Num) + ", " +str(self.Student.Class) + ", " + str(self.Student.Section)

    def clean(self):
        # print("clean")
        if self.Theory_Marks > 70.00 or self.Practical_Marks > 30.00 or self.Percentage > 100.00:
            raise ValidationError(
                "Input Marks is invalid, must be less than or equal to Max_Marks")
