from django.conf.urls import url
from exam import views


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^create_exam_record/$', views.create_exam_record, name='create_exam_record'),
    url(r'^save_exam_records/$', views.save_exam_records, name='save_exam_records'),
    url(r'^student_exam_edit/(?P<pk>\d+)$', views.student_exam_edit, name='student_exam_edit'),
]