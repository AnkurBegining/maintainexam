from django.core.exceptions import PermissionDenied
from employee.models import Employee
from cadetparent.models import CadetParent


def user_is_employee(function):
    def wrap(request, *args, **kwargs):
        users = Employee.objects.filter(user=request.user)
        if users:
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

def user_is_cadet_or_parent(function):
    def wrap(request, *args, **kwargs):
        users = CadetParent.objects.filter(user=request.user)
        if users:
            return function(request, *args, **kwargs)
        else:
            raise PermissionDenied

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap