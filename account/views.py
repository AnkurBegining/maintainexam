from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from django.contrib.auth.models import User
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.forms import AuthenticationForm
import smtplib
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from account.forms import SignUpForm, MobileForm
from cadetparent.models import CadetParent
from employee.models import Employee
from account.tokens import account_activation_token
from django.contrib.auth import login


# Create your views here.

# ------------For Users -------------
def signup(request):
    if request.method == 'POST':
        user_form = SignUpForm(request.POST)
        mobile_form = MobileForm(request.POST)

        if user_form.is_valid() and mobile_form.is_valid():
            user = user_form.save(commit=False)
            user.is_active = False
            user.save()
            user.mobile.Mobile_Number = mobile_form.cleaned_data.get('Mobile_Number')
            user.mobile.save()
            username = user_form.cleaned_data.get('username')
            users = get_object_or_404(User, username=username)
            cadet_parent = CadetParent.objects.create(user=users, is_cadet_parent=True)
            cadet_parent.save()
            current_site = get_current_site(request)
            print("current site", current_site)
            subject = 'Activate Your Account'
            message = render_to_string('account/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            print("current site1", current_site)
            user.email_user(subject, message)
            return render(request, 'account/account_activation_sent.html')
        else:
            print(user_form)
            redirect(request.META['HTTP_REFERER'])

    else:
        user_form = SignUpForm()
        mobile_form = MobileForm()
    context = {
        "user_form": user_form,
        "Mobile_form": mobile_form,
        'errors': user_form.errors,
    }
    return render(request, 'account/account_registrations.html', context)


def account_activation_sent(request):
    return render(request, 'account/account_activation_sent.html')


def activate(request, uidb64, token, ):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.mobile.email_confirmed = True
        user.mobile.save()
        user.save()
        login(request, user, backend='django.core.mail.backends.smtp.EmailBackend')
        return redirect('exam:home')
    else:
        return render(request, 'account/account_activation_invalid.html')


# Do change this when in production
email_address = 'evicharya@gmail.com'
email_password = 'qwerty@1234'


def send_verification_mail(email, msg, sub):
    # print("send verification mail")
    try:
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.login(email_address, email_password)
        server.sendmail(email_address, email, msg, sub)
        server.close()
        # print('successfully sent the mail')

    except:
        print("failed to send mail")


def Login(request):
    error_message = ""

    if request.method == 'POST':
        login_form = AuthenticationForm(request.POST or None)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                # To-do change the redirect to usertype
                return redirect('accountregistrations:Usertype')
        else:

            error_message = "Incorrect username or password."
            messages.error(request, error_message)
            login_form = AuthenticationForm(request.POST or None)

    else:
        login_form = AuthenticationForm()
    return render(request, 'account/account_registrations.html',
                  {'login_form': login_form, 'error_message': error_message})


@login_required
def Usertype(request):
    employee = Employee.objects.filter(user=request.user)
    if employee:
        return redirect('employee:employee_profile')
    return redirect('cadetparent:cadet_parent_profile')
